import uvicorn
from fastapi import FastAPI

from app.db.base import Base, engine
from app.todo.views import router as todo_router
from app.users.views import router as users_router

app = FastAPI()
app.include_router(users_router, prefix='/users', tags=['Users'])
app.include_router(todo_router, prefix='/todo', tags=['Todo'])

Base.metadata.create_all(bind=engine)

if __name__ == '__main__':
    uvicorn.run('main:app', reload=True)
