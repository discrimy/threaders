from typing import NewType

from sqlalchemy import Column, Integer, String, ForeignKey, Boolean
from sqlalchemy.orm import relationship

from app.db.base import Base


class User(Base):
    __tablename__ = 'user'

    id = Column(Integer, primary_key=True, index=True)
    username = Column(String(32), index=True, unique=True)
    hashed_password = Column(String)

    todolists = relationship('TodoList', back_populates='user')


UserID = NewType('UserID', int)


class TodoList(Base):
    __tablename__ = 'todolist'

    id = Column(Integer, primary_key=True, index=True)
    title = Column(String(32))

    user_id = Column(Integer, ForeignKey(User.id))
    user = relationship('User', back_populates='todolists')
    items = relationship('TodoItem', back_populates='todolist')


TodoListID = NewType('TodoListID', int)


class TodoItem(Base):
    __tablename__ = 'todoitem'

    id = Column(Integer, primary_key=True, index=True)
    title = Column(String(32))
    description = Column(String(256), default='')
    done = Column(Boolean, default=False)

    todolist_id = Column(Integer, ForeignKey(TodoList.id))
    todolist = relationship('TodoList', back_populates='items')


TodoItemID = NewType('TodoItemID', int)
