from sqlalchemy import create_engine
from sqlalchemy.orm import declarative_base, sessionmaker

from app import settings

engine = create_engine(settings.DATABASE_URL, echo=True)
Base = declarative_base()
Session = sessionmaker(bind=engine)
