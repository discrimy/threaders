from http import HTTPStatus
from typing import Optional

from fastapi import APIRouter, Depends
from pydantic import BaseModel

import app.todo.selectors
from app.db.base import Session
from app.db.entities import User, TodoListID, TodoItemID
from app.todo import services as todo_services
from app.common.deps import get_user, get_session
from app.common.responses import TodoItemResponse, TodoListResponse
from app.common.shortcuts import EmptyResponse

router = APIRouter()


class CreateTodoListForm(BaseModel):
    title: str


@router.post('/', status_code=HTTPStatus.CREATED, response_model=TodoListResponse)
async def create_todolist(
        form: CreateTodoListForm,
        session: Session = Depends(get_session),
        current_user: User = Depends(get_user),
):
    todolist_id = await todo_services.create_todolist(
        session,
        current_user.id,
        form.title,
    )

    todolist = await app.todo.selectors.get_todolist(session, todolist_id, current_user.id)
    return TodoListResponse.from_orm(todolist)


class CreateTodoItemForm(BaseModel):
    title: str
    description: str = ''


@router.post('/{todolist_id}/', status_code=HTTPStatus.CREATED, response_model=TodoItemResponse)
async def create_todoitem(
        form: CreateTodoItemForm,
        todolist_id: TodoListID,
        session: Session = Depends(get_session),
        current_user: User = Depends(get_user),
):
    todoitem_id = await todo_services.create_todoitem(
        session, todolist_id, current_user.id, form.title, form.description
    )

    todoitem = await app.todo.selectors.get_todoitem(session, todoitem_id, todolist_id, current_user.id)
    return TodoItemResponse.from_orm(todoitem)


class EditTodoItemForm(BaseModel):
    done: Optional[bool]
    title: Optional[str]
    description: Optional[str]


@router.patch('/{todolist_id}/{todoitem_id}/', response_model=TodoItemResponse)
async def edit_todoitem(
        todolist_id: TodoListID,
        todoitem_id: TodoItemID,
        form: EditTodoItemForm,
        session: Session = Depends(get_session),
        current_user: User = Depends(get_user),
):
    await todo_services.edit_todoitem(
        session,
        todoitem_id,
        todolist_id,
        current_user.id,
        title=form.title,
        description=form.description,
        done=form.done,
    )

    todoitem = await app.todo.selectors.get_todoitem(session, todoitem_id, todolist_id, current_user.id)
    return TodoItemResponse.from_orm(todoitem)


@router.delete('/{todolist_id}/{todoitem_id}/', status_code=HTTPStatus.NO_CONTENT)
async def delete_todoitem(
        todolist_id: TodoListID,
        todoitem_id: TodoItemID,
        session: Session = Depends(get_session),
        current_user: User = Depends(get_user),
):
    await todo_services.delete_todoitem(session, todoitem_id, todolist_id, current_user.id)

    return EmptyResponse()
