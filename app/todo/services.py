from typing import Optional

from sqlalchemy.orm import Session

import app.users.selectors as users_selectors
from app.db.entities import User, TodoList, TodoItem, UserID, TodoListID, TodoItemID
from app.todo.selectors import get_todolist, get_todoitem


async def create_todolist(
        session: Session,
        user_id: UserID,
        title: str,
) -> TodoListID:
    user: User = await users_selectors.get_user_by_id(session, user_id)

    todolist = TodoList(
        title=title,
        user=user,
    )
    session.flush()

    return TodoListID(todolist.id)


async def create_todoitem(
        session: Session,
        todolist_id: TodoListID,
        user_id: UserID,
        title: str,
        description: str,
) -> TodoItemID:
    todolist: TodoList = await get_todolist(session, todolist_id, user_id)

    todoitem = TodoItem(
        title=title,
        description=description,
        todolist=todolist,
    )
    session.flush()

    return todoitem.id


async def edit_todoitem(
        session: Session,
        todoitem_id: TodoItemID,
        todolist_id: TodoListID,
        user_id: UserID,
        *,
        title: Optional[str] = None,
        description: Optional[str] = None,
        done: Optional[bool] = None,
) -> None:
    todoitem: TodoItem = await get_todoitem(session, todoitem_id, todolist_id, user_id)

    if done is not None:
        todoitem.done = done
    if title is not None:
        todoitem.title = title
    if description is not None:
        todoitem.description = description
    session.flush()


async def delete_todoitem(
        session: Session,
        todoitem_id: TodoItemID,
        todolist_id: TodoListID,
        user_id: UserID,
) -> None:
    todoitem: TodoItem = await get_todoitem(session, todoitem_id, todolist_id, user_id)

    session.delete(todoitem)
    session.flush()
