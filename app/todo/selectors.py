from sqlalchemy import and_
from sqlalchemy.orm import Session

from app.common.services import not_none_or_404
from app.db.entities import TodoListID, UserID, TodoList, TodoItemID, TodoItem


async def get_todolist(
        session: Session,
        todolist_id: TodoListID,
        user_id: UserID,
) -> TodoList:
    cached_todolist = session.get(TodoList, todolist_id)
    if cached_todolist is None:
        # SQL-based filters
        cached_todolist = session.query(TodoList).filter(
            and_(TodoList.id == todolist_id, TodoList.user_id == user_id)
        ).first()
    else:
        # ORM-based filters
        if cached_todolist.user_id != user_id:
            cached_todolist = None

    return not_none_or_404(cached_todolist)


async def get_todoitem(
        session: Session,
        todoitem_id: TodoItemID,
        todolist_id: TodoListID,
        user_id: UserID,
) -> TodoItem:
    return await not_none_or_404(
        session.query(TodoItem).filter(
            and_(
                TodoItem.id == todoitem_id,
                TodoList.id == todolist_id,
                TodoList.user_id == user_id,
            )
        ).join(TodoList).first()
    )