from enum import Enum
from typing import Generic, TypeVar, Union

from pydantic import BaseModel


class AnswerStatus(str, Enum):
    SUCCESS = 'success'
    ERROR = 'error'


class AnswerError(BaseModel):
    status: AnswerStatus = AnswerStatus.ERROR
    message: str = 'No message provided'

    @classmethod
    def from_error(cls, error: Exception) -> 'AnswerError':
        return cls(
            message=error.args[0] if error.args else 'No message provided'
        )


D = TypeVar('D', bound=BaseModel)


class AnswerSuccess(BaseModel, Generic[D]):
    status: AnswerStatus = AnswerStatus.SUCCESS
    data: D


Answer = Union[AnswerSuccess, AnswerError]
