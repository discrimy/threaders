from http import HTTPStatus

from starlette.responses import Response


class EmptyResponse(Response):
    """
    Shortcut for empty response.

    Usage:
    Return it from view with `return EmptyResponse()`.
    Do not forget to set `status_code=HTTPStatus.NO_CONTENT`
    in view's config.
    """

    def __init__(self):
        super().__init__(status_code=HTTPStatus.NO_CONTENT)
