from typing import List

from fastapi_camelcase import CamelModel


class TodoItemResponse(CamelModel):
    id: int
    title: str
    description: str
    done: bool

    class Config:
        orm_mode = True


class TodoListResponse(CamelModel):
    id: int
    title: str
    items: List[TodoItemResponse]

    class Config:
        orm_mode = True


class ShortUserResponse(CamelModel):
    id: int
    username: str

    class Config:
        orm_mode = True


class FullUserResponse(CamelModel):
    id: int
    username: str
    todolists: List[TodoListResponse]

    class Config:
        orm_mode = True


class AllPublicUsersResponse(CamelModel):
    users: List[ShortUserResponse]
