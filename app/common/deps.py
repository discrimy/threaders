from fastapi import Depends
from fastapi.security import HTTPBasic, HTTPBasicCredentials

from app.db.base import Session
from app.users import services as user_services


async def get_session():
    with Session.begin() as session:
        yield session


security = HTTPBasic()


async def get_user(
        session: Session = Depends(get_session),
        credentials: HTTPBasicCredentials = Depends(security),
):
    return await user_services.auth_user(session, credentials.username, credentials.password)
