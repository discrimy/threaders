from typing import TypeVar, Optional

from app.common.errors import DomainError


class NotFoundError(DomainError):
    def __init__(self):
        super().__init__(f'Entity not found')


T = TypeVar('T')


def not_none_or_404(value: Optional[T]) -> T:
    if value is None:
        raise NotFoundError()
    return value
