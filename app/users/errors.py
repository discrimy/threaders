from app.common.errors import DomainError


class UserWithSuchUsernameAlreadyExistsError(DomainError):
    def __init__(self, username: str):
        super().__init__(f'User with username "{username}" already exists')


class WrongPasswordError(DomainError):
    def __init__(self):
        super().__init__(f'Wrong password was provided')