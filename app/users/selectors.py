from typing import List

from app.common.services import not_none_or_404
from app.db.base import Session
from app.db.entities import User, UserID


async def get_user_by_username(session: Session, username: str) -> User:
    return not_none_or_404(session.query(User).filter(User.username == username).first())


async def get_user_by_id(session: Session, user_id: UserID) -> User:
    return not_none_or_404(session.get(User, user_id))


async def get_all_users(session: Session) -> List[User]:
    return session.query(User).order_by(User.username.asc()).all()
