import bcrypt

from app.db.base import Session
from app.db.entities import User, UserID
from app.users.errors import UserWithSuchUsernameAlreadyExistsError, WrongPasswordError
from app.users.selectors import get_user_by_username


async def auth_user(session: Session, username: str, password: str) -> User:
    user = await get_user_by_username(session, username)
    if not bcrypt.checkpw(password.encode(), user.hashed_password):
        raise WrongPasswordError()
    return user


async def _is_user_exists_by_username(session: Session, username: str) -> bool:
    return session.query(User.id).filter(User.username == username).first() is not None


async def create_user(session: Session, username: str, password: str) -> UserID:
    if await _is_user_exists_by_username(session, username):
        raise UserWithSuchUsernameAlreadyExistsError(username)

    hashed_password = bcrypt.hashpw(password.encode(), bcrypt.gensalt())
    user = User(
        username=username,
        hashed_password=hashed_password,
    )
    session.add(user)

    return user.id
