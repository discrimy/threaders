from http import HTTPStatus

from fastapi import APIRouter, Depends, HTTPException
from starlette.responses import Response

import app.users.selectors
from app.db.base import Session
from app.db.entities import User
from app.users import services as user_services
from app.common.errors import DomainError
from app.common.deps import get_session, get_user
from app.common.responses import ShortUserResponse, FullUserResponse, AllPublicUsersResponse

router = APIRouter()


@router.post('/{username}/test/', status_code=HTTPStatus.OK)
async def test_auth(username: str, password: str, session: Session = Depends(get_session)):
    try:
        await user_services.auth_user(session, username, password)
    except DomainError as error:
        raise HTTPException(status_code=HTTPStatus.BAD_REQUEST, detail=error.message)

    return Response()


@router.post('/', status_code=HTTPStatus.CREATED, response_model=ShortUserResponse)
async def create_user(username: str, password: str, session: Session = Depends(get_session)):
    try:
        await user_services.create_user(session, username, password)
    except DomainError as error:
        raise HTTPException(status_code=HTTPStatus.BAD_REQUEST, detail=error.message)

    user = await app.users.selectors.get_user_by_username(session, username)
    return ShortUserResponse.from_orm(user)


@router.get('/', response_model=AllPublicUsersResponse)
async def get_all_users(session: Session = Depends(get_session)):
    users = await app.users.selectors.get_all_users(session)
    return AllPublicUsersResponse(
        users=[ShortUserResponse.from_orm(user) for user in users]
    )


@router.get('/me/', response_model=FullUserResponse)
async def get_me(current_user: User = Depends(get_user)):
    return FullUserResponse.from_orm(current_user)
